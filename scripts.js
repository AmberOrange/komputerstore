// Declare variables
let salary = 100
let balance = 200
let pay = 0
let payRate = 50
let currentLoanCount = 0
let currentLoanDebt = 0
let laptopCount = 0
let currentSelectedLaptop = 0
// Get references for ease-of-access
let balanceRef = document.getElementById("balance")
let payRef = document.getElementById("pay")

// To be called at the beginning of the page
populateDropDown()
balanceRef.innerText = balance
payRef.innerText = pay

// Restore the modal to its normal look before showing it
$('#loanModal').on('show.bs.modal', function() { 
    restoreLoanAmountError()
    document.getElementById("loanAmount").value = ""
});

// Auto-focus on the input field once the modal is loaded
$('#loanModal').on('shown.bs.modal', function() { 
    document.getElementById("loanAmount").focus()
});

// Get paid and update the displayed amount
function clickWork() {
    pay += payRate
    payRef.innerText = pay
}

// Transfer pay to balance and update displayed amount
function clickBank() {
    balance += pay
    pay = 0

    balanceRef.innerText = balance
    payRef.innerText = pay
}

// Called once the user has entered a requested loan amount and clicked the submit button
// This function is called as a "onsubmit", which would normally redirect the page,
// but by returning false we can avoid that
function clickRequestLoan() {
    // Check if the user already has a loan for the first computer
    if(laptopCount == 0 && currentLoanCount > 0) {
        loanAmountError("You cannot get more than one bank loan before buying a computer!")
        return false
    }
    // Get the requested amount
    let loanAmount = parseInt(document.getElementById("loanAmount").value)

    // Check whether the user entered a valid number
    if(isNaN(loanAmount)) {
        loanAmountError("Please provide a number")
        return false
    // Also, check if the user has enough money in the bank for the request
    } else if(balance * 2 < loanAmount) {
        loanAmountError("Loan request too high! Maximum: " + (balance*2))
        return false
    }

    // Do the loan transaction
    balance += loanAmount
    currentLoanDebt += loanAmount
    currentLoanCount++

    // Update the balance counter on the page
    balanceRef.innerText = balance

    // If there was any previous errors, restore them and hide the modal
    restoreLoanAmountError()
    $("#loanModal").modal("hide")
    return false
}

// This function makes the amount input turn red and display an error message
function loanAmountError(message) {
    // Get references for convenience
    let loanAmountRef = document.getElementById("loanAmount")
    let loanAmountErrorRef = document.getElementById("loanAmountError")

    // Add styling to input
    loanAmountRef.classList.add("is-invalid")

    // Show error message
    loanAmountErrorRef.style.display = "initial"
    loanAmountErrorRef.innerText = message
}

// Restores the modal to normal, removing all error styling
function restoreLoanAmountError() {
    // Get references for convenience
    let loanAmountRef = document.getElementById("loanAmount")
    let loanAmountErrorRef = document.getElementById("loanAmountError")

    // Remove styling to input
    loanAmountRef.classList.remove("is-invalid")

    // Hide error message
    loanAmountErrorRef.style.display = "none"
}

// This function adds all elements from the "laptopData" json into a dropdown list
function populateDropDown() {
    // Get reference for convenience
    let dropdownRef = document.getElementById("dropdownList")
    // Empty out previous list
    dropdownRef.innerHTML = ""
    // For every element in laptopData...
    for(let i = 0; i < laptopData.length; i++) {
        // Make a new DOM button
        let tag = document.createElement("button")
        // Add attribute and styling
        tag.classList.add("dropdown-item")
        tag.setAttribute("type", "button")
        tag.setAttribute("onclick",`clickDropdown(${i})`)

        // Add text to the dropdown button
        let text = document.createTextNode(laptopData[i].name)
        tag.appendChild(text)

        // Add to the dropdown list
        dropdownRef.appendChild(tag)
    }
    // Set the first laptop as the default shown laptop
    clickDropdown(0)
}

// Goes through all elements in the selected laptop json object and
// writes the information in the appropriate locations
function clickDropdown(i) {
    document.getElementById("dropdownButton").innerText = laptopData[i].name
    document.getElementById("laptopFeatures").innerText = laptopData[i].feat
    document.getElementById("laptopHeader").innerText = laptopData[i].name
    document.getElementById("laptopDescription").innerText = laptopData[i].desc
    document.getElementById("laptopPrice").innerText = laptopData[i].price
    document.getElementById("laptopImage").src = laptopData[i].image

    // Set the current selected laptop
    currentSelectedLaptop = i
    // If the laptop is already bought since before,
    // make the button look disabled
    setBuyButtonDisabled(laptopData[i].hasOwnProperty("bought"))
}

// A function to style the "buy" button to look disabled
// in case the laptop has or have been bought
function setBuyButtonDisabled(disable) {
    let buyButton = document.getElementById("buyButton")
    if(disable) {
        buyButton.classList.add("disabled")
        buyButton.classList.remove("btn-success")
        buyButton.classList.add("btn-secondary")
        buyButton.innerText="Owned"
    } else {
        buyButton.classList.remove("disabled")
        buyButton.classList.remove("btn-secondary")
        buyButton.classList.add("btn-success")
        buyButton.innerText="Buy"
    }
}

// When the user clicks the buy button, attempt to buy the currently selected laptop
function clickBuy() {
    // Get reference for convenience
    let laptop = laptopData[currentSelectedLaptop];
    // Check whether the laptop has already been bought before
    if(laptop.hasOwnProperty("bought")) {
        buyMessage("You already own this laptop", false)
    // Check if the user has enough money on their bank account
    } else if(laptop.price > balance) {
        buyMessage("You can't afford this laptop with your current balance", false)
    // Otherwise, it's time to for the transaction!
    } else {
        balance -= laptop.price
        balanceRef.innerText = balance
        laptop.bought = true
        laptopCount++

        // Set the button to disabled
        setBuyButtonDisabled(true)

        // Show them the successful buy message
        buyMessage(`You succesfully bought a ${laptop.name}!`, true)
    }
}

// Prints a buy message
// if success is false, show it as an error message
function buyMessage(message, success) {
    if(success) {
        document.getElementById("buyModalLabel").innerText="Buy successful!"
    } else {
        document.getElementById("buyModalLabel").innerText="Something happened..."
    }
    document.getElementById("buyModalMessage").innerText=message
    $('#buyModal').modal('show')
}