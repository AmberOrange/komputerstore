let laptopData =
[
    {
        name:"Epson's HX-20",
        desc:"The screen is a tiny built-in monochrome LCD capable of displaying four lines of text at a time. It also has a brown plastic case that close over the keyboard, a built-in dot-matrix printer that spit out adding-machine paper, and a micro-cassette tape drive for storage.",
        feat:"Portable\nTechnically a laptop\n50 hour battery",
        image:"img/epson.png",
        price:1500
    },
    {
        name:"Tandy's TRS-80 Model",
        desc:"This operates on four AA batteries for up to 18 hours usage. It has no internal storage (an external cassette recorder or a 5.25-inch floppy drive cost extra), but it offers a raft of built-in applications, including a text editor, an address book, a scheduler, and a telecom application for modem communications.",
        feat:"Quite popular\nCheap..?",
        image:"img/tandy.png",
        price:4000
    },
    {
        name:"Compaq's Portable 386",
        desc:"The Portable 386 may not be pretty, but its Intel 80386DX-20 chip operating at ascorching 20MHz makes it one of the fastest portable computers on the planet (at the time)",
        feat:"Hosts a powerful Intel 80386 processor\n32-bit computing",
        image:"img/compaq.png",
        price:6000
    },
    {
        name:"GRiD Systems' 2260",
        desc:"So you thought handwriting recognition was an invention of the new millennium? Nope. GRiD Systems' Convertible model 2260 (also marketed by AST as the PenExec) is the first notebook with a pen-sensitive screen that can pivot and lie flat against the keyboard for use as a slate",
        feat:"Tablet with a pen-sensitive screen!\nPowerful processor inside",
        image:"img/grid.jpg",
        price:10000
    }
]
